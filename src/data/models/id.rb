# Created by Matyáš Pokorný on 2019-06-19.

module ROM
	class IdModel < Model
		property! :id, Integer
	end
end