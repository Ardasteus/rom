# Created by Matyáš Pokorný on 2019-05-20.

module ROM
	# Marks property as to be automatically supplied by DB
	class AutoAttribute < Attribute
	end
end